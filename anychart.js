jQuery(document).ready(function() {
	// Ukrywamy pola, które mają być dostępne, tylko gdy chcemy dodać nową stronę
	
	if ( jQuery('input:radio[name="anychart_data_source[und]"]:checked').val() == 'manual') {
		jQuery('ul.vertical-tabs-list li:nth-child(2)').show();
		jQuery('ul.vertical-tabs-list li:nth-child(3)').hide();
		jQuery('ul.vertical-tabs-list li:nth-child(4)').hide();
	} else if (jQuery('input:radio[name="anychart_data_source[und]"]:checked').val() == 'xml') {
		jQuery('ul.vertical-tabs-list li:nth-child(3)').show();
		jQuery('ul.vertical-tabs-list li:nth-child(2)').hide();
		jQuery('ul.vertical-tabs-list li:nth-child(4)').hide();
	} else if (jQuery('input:radio[name="anychart_data_source[und]"]:checked').val() == 'xmlurl') {
		jQuery('ul.vertical-tabs-list li:nth-child(4)').show();
		jQuery('ul.vertical-tabs-list li:nth-child(2)').hide();
		jQuery('ul.vertical-tabs-list li:nth-child(3)').hide();
	}
	
	// Pokazujemy / ukrywamy pola w zależności czy użytkownik chce dodać nową stronę
	jQuery('input:radio[name="anychart_data_source[und]"]').click(function() {
		if (jQuery(this).val() == 'manual') {
			jQuery('ul.vertical-tabs-list li:nth-child(2)').show();
			jQuery('ul.vertical-tabs-list li:nth-child(3)').hide();
			jQuery('ul.vertical-tabs-list li:nth-child(4)').hide();
		} else if (jQuery(this).val() == 'xml') {
			jQuery('ul.vertical-tabs-list li:nth-child(3)').show();
			jQuery('ul.vertical-tabs-list li:nth-child(2)').hide();
			jQuery('ul.vertical-tabs-list li:nth-child(4)').hide();
		} else if (jQuery(this).val() == 'xmlurl') {
			jQuery('ul.vertical-tabs-list li:nth-child(4)').show();
			jQuery('ul.vertical-tabs-list li:nth-child(2)').hide();
			jQuery('ul.vertical-tabs-list li:nth-child(3)').hide();
		}
		
		//if ( jQuery('#edit-anychart-data-source-und-manual').is(':checked') ) {
			//jQuery('ul.vertical-tabs-list li:nth-child(2)').show();
		//} else {
			//jQuery('ul.vertical-tabs-list li:nth-child(2)').hide();
		//}
	});
});