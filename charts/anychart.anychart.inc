<?php 
/**
 * Abstract Anychart class, which defines common settings and data of charts.
 * Uses Array2XML - a class to convert array in PHP to XML
 * (http://www.lalit.org/lab/convert-php-array-to-xml-with-attributes).
 */
require_once drupal_get_path( 'module', 'anychart' ) . '/charts/anychart.array2xml.inc';

abstract class AnyChart extends Array2XML {
	
	// Variables
	protected $node;
	protected $series;
	protected $chart_type;
	protected $plot_type;
	protected $chart;
	
	// Functions
	public function __construct(&$node) {
		$chart = taxonomy_term_load($node->anychart_chart_type[LANGUAGE_NONE][0]['tid']);

		$this->node 		  = $node;
		$this->chart_type = $chart->anychart_term_chart_type[LANGUAGE_NONE][0]['value'];
		$this->plot_type  = $chart->anychart_term_plot_type[LANGUAGE_NONE][0]['value'];
		
		$this->getDataSeries();
		$this->parseUserData();
	}
	public function getXML() {
		//dpm($this->node);
		//dpm(parent::createXML('anychart', $this->chart)->saveXML());
		return parent::createXML('anychart', $this->chart)->saveXML();
	}
	
	// Generate chart array functions
	protected function parseUserData() {
		// General
		$this->chart['charts'] = array();
		$this->getTagChart();
		$this->getTagDataPlotSettings();
		$this->getTagData();
		// Series
		$this->getTagSeries();
		$this->getTagPoint();
		// Settings
		$this->getTagChartSettings();
		$this->getTagChartTitle();
		$this->getTagAxes();
		$this->getTagAxis('x');
		$this->getTagAxis('y');
		$this->getTagAxisTitle('x');
		$this->getTagAxisTitle('y');
		$this->getTagLegend();
		
	}
	protected function getTagChart() {
		if ($this->plot_type != '_none') {
			$this->chart['charts']['chart']['@attributes']['plot_type'] = $this->plot_type;
		}
	}
	protected function getTagDataPlotSettings() {
		if ($this->chart_type != '_none') {
			$this->chart['charts']['chart']['data_plot_settings']['@attributes']['default_series_type'] = $this->chart_type;
		}
		// 3D
		if ($this->node->anychart_3d[LANGUAGE_NONE][0]['value'] == 1) {
			$this->chart['charts']['chart']['data_plot_settings']['@attributes']['enable_3d_mode'] = 'true';
		}
	}
	protected function getTagData() {
		$this->chart['charts']['chart']['data'] = array();
	}
	protected function getTagSeries() {		
		foreach ($this->series as $delta => $series) {
			$this->chart['charts']['chart']['data']['series'][$delta]['@attributes']['name'] = $series['title'];
			if ($this->chart_type != '_none') {
				$this->chart['charts']['chart']['data']['series'][$delta]['@attributes']['type'] = $this->chart_type;
			}
		}
	}
	protected function getTagPoint() {
		foreach ($this->series as $sdelta => $series) {
		  foreach ($this->series[$sdelta]['rows'] as $rdelta => $row) {
		  	$this->chart['charts']['chart']['data']['series'][$sdelta]['point'][$rdelta]['@attributes']['name'] = $this->node->anychart_labels[LANGUAGE_NONE][$rdelta]['value'];
		  }
		}
	}
	protected function getTagChartSettings() {
		$this->chart['charts']['chart']['chart_settings'] = array();
	}
	protected function getTagChartTitle() {
		$enabled = ($this->node->anychart_chart_title[LANGUAGE_NONE][0]['value'] == 1);
		$this->chart['charts']['chart']['chart_settings']['title']['@attributes']['enabled'] = $enabled ? 'true' : 'false';
		
		if ($enabled) {
			$this->chart['charts']['chart']['chart_settings']['title']['text'] = array('@cdata' => $this->node->title);
		}
	}
	protected function getTagAxes() {
		$this->chart['charts']['chart']['chart_settings']['axes'] = array();
	}
	protected function getTagAxis($axis = 'x') {
		$this->chart['charts']['chart']['chart_settings']['axes'][$axis . '_axis'] = array();
	}
	protected function getTagAxisTitle($axis = 'x') {
		$field_name = 'anychart_' . $axis . 'axis_label';
		
		if (!empty($this->node->{$field_name})) {
			$this->chart['charts']['chart']['chart_settings']['axes'][$axis . '_axis']['title']['text'] = array('@cdata' => $this->node->{$field_name}[LANGUAGE_NONE][0]['value']);
		}
	}
	protected function getTagLegend() {
		$enabled = ($this->node->anychart_legend_enabled[LANGUAGE_NONE][0]['value'] == 1);
		
		if ($enabled) {
			$this->chart['charts']['chart']['chart_settings']['legend']['@attributes']['enabled'] = $enabled ? 'true' : 'false';
			// Legend label
			if (!empty($this->node->anychart_legend_label[LANGUAGE_NONE])) {
				$this->chart['charts']['chart']['chart_settings']['legend']['title'] = array(
					'@attributes' => array(
						'enabled' => 'true',
					),
					'text' => array('@cdata' => $this->node->anychart_legend_label[LANGUAGE_NONE][0]['value']),
				);
			} else {
				$this->chart['charts']['chart']['chart_settings']['legend']['title']['@attributes']['enabled'] = 'false';
			}
			// Legend Position
			if ($this->node->anychart_legend_position[LANGUAGE_NONE][0]['value'] != '_none') {
				$this->chart['charts']['chart']['chart_settings']['legend']['@attributes']['position'] = $this->node->anychart_legend_position[LANGUAGE_NONE][0]['value'];
			}
			// Legend width and height
			if ($this->node->anychart_legend_width_mode[LANGUAGE_NONE][0]['value'] == 0) {
				$this->chart['charts']['chart']['chart_settings']['legend']['@attributes']['width'] = $this->node->anychart_legend_width[LANGUAGE_NONE][0]['value'];
			}
			if ($this->node->anychart_legend_height_mode[LANGUAGE_NONE][0]['value'] == 0) {
				$this->chart['charts']['chart']['chart_settings']['legend']['@attributes']['height'] = $this->node->anychart_legend_height[LANGUAGE_NONE][0]['value'];
			}
		}
	}
	
	// Helper functions
	protected function getDataSeries() {
		$this->series = array();
		
		// Get series
		foreach ($this->node->anychart_data_collection[LANGUAGE_NONE] as $sdelta => $series) {
			if (!is_numeric($sdelta)) {
				continue;
			}
			$this->series[$sdelta]['title'] = $series['anychart_series_title'][LANGUAGE_NONE][0]['value'];
			// Get rows
			foreach ($series['anychart_data_row'][LANGUAGE_NONE] as $rdelta => $row) {
				if (!is_numeric($rdelta)) {
					continue;
				}
				$this->series[$sdelta]['rows'][$rdelta]['anychart_data_row_x'] = $row['anychart_data_row_x'][LANGUAGE_NONE][0]['value'];
				$this->series[$sdelta]['rows'][$rdelta]['anychart_data_row_y'] = $row['anychart_data_row_y'][LANGUAGE_NONE][0]['value'];
				$this->series[$sdelta]['rows'][$rdelta]['anychart_data_row_size'] = $row['anychart_data_row_size'][LANGUAGE_NONE][0]['value'];
				$this->series[$sdelta]['rows'][$rdelta]['anychart_data_row_start'] = $row['anychart_data_row_start'][LANGUAGE_NONE][0]['value'];
				$this->series[$sdelta]['rows'][$rdelta]['anychart_data_row_end'] = $row['anychart_data_row_end'][LANGUAGE_NONE][0]['value'];
				$this->series[$sdelta]['rows'][$rdelta]['anychart_data_row_color'] = $row['anychart_data_row_color'][LANGUAGE_NONE][0]['value'];
			}
		}
	}
	protected function getEids($fields) {
		$eids = array();
		foreach ($fields as $eid) {
			$eids[] = $eid['value'];
		}
		return $eids;
	}
}