<?php 
require_once drupal_get_path( 'module', 'anychart' ) . '/charts/anychart.anychart.inc';

/**
 * BarChart chart type implementation.
 */
class BubbleChart extends AnyChart {

	// Functions
	public function __construct(&$node) {
		parent::__construct($node);
	}
	
	// Generate chart array functions
	protected function getTagPoint() {
		parent::getTagPoint();
		
		foreach ($this->series as $sdelta => $series) {
			foreach ($this->series[$sdelta]['rows'] as $rdelta => $row) {
				$this->chart['charts']['chart']['data']['series'][$sdelta]['point'][$rdelta]['@attributes']['y'] = $row['anychart_data_row_y'];
				$this->chart['charts']['chart']['data']['series'][$sdelta]['point'][$rdelta]['@attributes']['size'] = $row['anychart_data_row_size'];
			}
		}
	}
}