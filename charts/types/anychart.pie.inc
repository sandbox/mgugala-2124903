<?php 
require_once drupal_get_path( 'module', 'anychart' ) . '/charts/anychart.anychart.inc';

/**
 * Pie/Daughnut Chart chart type implementation.
 */
class PieChart extends AnyChart {

	// Functions
	public function __construct(&$node) {
		parent::__construct($node);
	}	
	
	// Generate chart array functions
	protected function getTagDataPlotSettings() {
		parent::getTagDataPlotSettings();
		
		$this->chart['charts']['chart']['data_plot_settings']['pie_series']['@attributes']['apply_palettes_to'] = 'Points';
	}
	protected function getTagSeries() {
		parent::getTagSeries();
		
		foreach ($this->series as $delta => $series) {
			$this->chart['charts']['chart']['data']['series'][$delta]['@attributes']['pallete'] = 'Default';
		}
	}
	protected function getTagPoint() {
		parent::getTagPoint();
		
		foreach ($this->series as $sdelta => $series) {
			foreach ($this->series[$sdelta]['rows'] as $rdelta => $row) {
				$this->chart['charts']['chart']['data']['series'][$sdelta]['point'][$rdelta]['@attributes']['y'] = $row['anychart_data_row_y'];
			}
		}
	}
	protected function getTagAxes() {
		// Override parent function, axes no needed, so it's empty.
	}
	protected function getTagAxis($axis = 'x') {
		// Override parent function, axes no needed, so it's empty.
	}
	protected function getTagAxisTitle($axis = 'x') {
		// Override parent function, axes no needed, so it's empty.
	}
}