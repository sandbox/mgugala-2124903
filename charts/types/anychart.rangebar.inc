<?php 
require_once drupal_get_path( 'module', 'anychart' ) . '/charts/anychart.anychart.inc';

/**
 * Range/Spline Bar Chart chart type implementation.
 */
class RangeBarChart extends AnyChart {

	// Functions
	public function __construct(&$node) {
		parent::__construct($node);
	}	
	
	// Generate chart array functions
	protected function getTagPoint() {
		parent::getTagPoint();
		
		foreach ($this->series as $sdelta => $series) {
			foreach ($this->series[$sdelta]['rows'] as $rdelta => $row) {
				$this->chart['charts']['chart']['data']['series'][$sdelta]['point'][$rdelta]['@attributes']['start'] = $row['anychart_data_row_start'];
				$this->chart['charts']['chart']['data']['series'][$sdelta]['point'][$rdelta]['@attributes']['end'] = $row['anychart_data_row_end'];
				$this->chart['charts']['chart']['data']['series'][$sdelta]['point'][$rdelta]['@attributes']['color'] = $row['anychart_data_row_color'];
			}
		}
	}
}