<?php
/*
 * Chart types
 */
foreach(scandir(drupal_get_path( 'module', 'anychart' ) . '/charts/types/') as $f) {
	if (is_file(drupal_get_path( 'module', 'anychart' ) . '/charts/types/' . $f) &&  substr(strrchr($f,'.'),1) == 'inc') {
		require_once drupal_get_path( 'module', 'anychart' ) . '/charts/types/' . $f;
	}
}
/**
 * Creates and manages chart of given type and generates its XML representation.
 */
class ChartCreator {
	private $chart;
	/**
	 * Creates chart with given type
	 * @param node node object 
	 */
	public function __construct(&$node) {
		$chart_type = taxonomy_term_load($node->anychart_chart_type[LANGUAGE_NONE][0]['tid']);
		$this->chart = new $chart_type->anychart_term_class_name[LANGUAGE_NONE][0]['value']($node);
	}
	/**
	 * Get chart data as XML
	 * 
	 * @return chart data XML
	 */
	public function getXML() {
		//dpm($this->chart->getXML());
		return $this->chart->getXML();
	}
}

