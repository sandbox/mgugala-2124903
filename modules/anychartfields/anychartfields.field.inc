<?php

function anychartfields_field_info() {
  return array(
    'anychart_xml_url' => array(
      'label' => t('Anychart XML URL'),
      'description' => t('Definies field to support Anychart library.'),
      'default_widget' => 'anychart_xml_url',
      'default_formatter' => 'anychart_xml_url',
    ),
  	'anychart_xml' => array(
  		'label' => t('Anychart XML'),
  		'description' => t('Definies field to support Anychart library.'),
  		'default_widget' => 'anychart_xml',
  		'default_formatter' => 'anychart_xml',
  	),
  );
}

function anychartfields_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, & $errors) {
	if ($field['type'] == 'anychart_xml_url') {
	  foreach ($items as $delta => $item) {
	    if (!empty($item['value'])) {
	      if (link_validate_url($item['value']) != LINK_EXTERNAL) {
	        $errors[$field['field_name']][$langcode][$delta][] = array(
	          'error'   => 'anychart_xml_url_invalid',
	          'message' => t('This URL must be an external address.')
	        );
	      }
	    }
	  }
	}
}

function anychartfields_field_is_empty($item, $field) {
  return empty($item['value']);
}

function anychartfields_field_formatter_info() {
  return array(
    'anychart_xml_url' => array(
      'label' => t('Default'),
      'field types' => array('anychart_xml_url'),
    ),
  	'anychart_xml' => array(
  		'label' => t('Default'),
  		'field types' => array('anychart_xml'),
  	),
  );
}

function anychartfields_field_widget_info() {
  return array(
    'anychart_xml_url' => array(
      'label'       => t('External URL to the XML file'),
      'field types' => array('anychart_xml_url')
    ),
  	'anychart_xml' => array(
  		'label'       => t('XML content'),
  		'field types' => array('anychart_xml')
  	)
  );
}

function anychartfields_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  switch ($display['type']) {
    case 'anychart_xml_url':
      foreach ($items as $delta => $item) {
      	if (isset($item['value'])) {
	        $element[$delta]['#markup'] = anychartfields_render_chart_from_xml_string(file_get_contents($item['value']));
      	}
      }
      break;
    case 'anychart_xml':
    	foreach ($items as $delta => $item) {
    		if (isset($item['value'])) {
      		$element[$delta]['#markup'] = anychartfields_render_chart_from_xml_string($item['value']);
      	}
      }
     	break;
  }
  return $element;
}

function anychartfields_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $value = isset($items[$delta]['value']) ? $items[$delta]['value'] : '';

  $widget = $element;
  $widget['#delta'] = $delta;

  switch ($instance['widget']['type']) {
    case 'anychart_xml_url':
      $widget += array(
        '#type' => 'textfield',
        '#title' => 'External URL to the XML file',
        '#default_value' => $value,
        '#description' => t('This value must be an external URL.'),
      );
    break;
    case 'anychart_xml':
      $widget += array(
        '#type' => 'textarea',
      	'#title' => 'XML content',
      	'#rows' => 10,
      	'#description' => t('This content must be XML.'),
      );
    break;
  }

  $element['value'] = $widget;
  return $element;
}